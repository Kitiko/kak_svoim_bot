<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/setWebhook', function () {
    $http = Http::get(
        env('BOT_URL')
        . env('TELEGRAM_TOKEN')
        . '/setWebhook?url='
        . env('NGROK_URL')
        . '/api/webhook?allowed_updates=["message"]'
    );
    dd(json_encode($http));
});

Route::get('/deleteWebhook', function () {
    $http = Http::get(
        env('BOT_URL')
        . env('REPORT_TELEGRAM_TOKEN')
        . '/deleteWebHook?drop_pending_updates=true');
    dd(json_decode($http));
});
