<?php

use App\Http\Controllers\ApiController;
use App\Http\Controllers\ApplicationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/tilda', [ApiController::class, 'getDataTilda']);

Route::post('/webhook', [ApplicationController::class, 'callback']);

