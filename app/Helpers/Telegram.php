<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class Telegram
{
    protected $http;

    protected $config;

    public function __construct()
    {
        $this->config= Config::get('bots.weather');
    }

    public function sendMessage($chat_id, $message)
    {
        return Http::post($this->config['url'] . $this->config['telegram_token'] . '/sendMessage',
            [
                'chat_id' => $chat_id,
                'text' => $message,
                'parse_mode' => 'html',
            ]
        );
    }

}
