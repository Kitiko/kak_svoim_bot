<?php

namespace App\Repositories;


use App\Models\City;
use App\Models\User;

class Repository
{
    protected City $model;

    public function __construct(City $model)
    {
        $this->model = $model;
    }

    public function getUtm_medium()
    {
        $utm = $this->model->all();
        return $utm[0]->id;
    }

    public function getStatus()
    {
        $status = User::all();
        return $status[0]->status;
    }

}
