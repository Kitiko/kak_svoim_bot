<?php

namespace App\Http\Controllers;

use App\Http\Requests\DataRequest;
use App\Models\City;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use TelegramBot\Api\BotApi;
use function Symfony\Component\String\s;

class ApiController extends Controller
{
    public function index()
    {
        return view('web');
    }

    public function getDataTilda(Request $data)
    {
        Log::debug($data);

        try {
            if ($data['Name'] && $data['Phone'] && $data['Город']) {
                $user = new User();
                $user->name = $data['Name'];
                $user->phone = $data['Phone'];
                $user->city = $data['Город'];
                $user->status = "1";

                $user->save();

                $this->getMessage($user);

                return response()->json(['success' => 'success'], 200);
            }

            if ($data['Name'] && $data['Phone'] && $data['Выберите_ваш_город']) {
                $user = new User();
                $user->name = $data['Name'];
                $user->phone = $data['Phone'];
                $user->city = $data['Выберите_ваш_город'];
                $user->status = "1";

                $user->save();

                $this->getMessage($user);

                return response()->json(['success' => 'success'], 200);
            }

            if ($data['Name'] && $data['Phone'] && $data['Выберите_ваш_город_для_более_точного_расчета']) {
                $user = new User();
                $user->name = $data['Name'];
                $user->phone = $data['Phone'];
                $user->city = $data['Выберите_ваш_город_для_более_точного_расчета'];
                $user->status = "1";

                $user->save();

                $this->getMessage($user);

                return response()->json(['success' => 'success'], 200);
            }
        }
        catch (\Exception $e){
            Log::debug($e);
        }

        return response()->json(['error' => 'not data'], 200);
    }

    public function getMessage(User $user)
    {

        $message = [
            'ФИО' => 'ФИО: ' . $user->name . "\n",
            'Телефон' => 'Телефон: ' . $user->phone,
        ];

        $telegram_channel = DB::table('telegram_channels')
            ->leftJoin('cities', 'cities.city', '=', 'telegram_channels.city')
            ->where('status', 1)
            ->where(function ($query) {
                $query->where('last_id', '<', 'telegram_channels.id')
                    ->orWhereNull('last_id');
            })
            ->where('telegram_channels.city', '=', $user->city)
            ->first();

        if ($telegram_channel->last_id == NULL){
            $cities = new City();
            $tg_channel = DB::table('telegram_channels')->where('city', $user->city)->where('status','=', 1)->first();

            if ($user->status > 0){
                $cities->city = $tg_channel->city;
                $cities->last_id = $tg_channel->id;
                $cities->save();

                $this->spam($tg_channel->tg_channel_id, $message);
            }
        }

        if (City::where('city', '=', $user->city)->first()->city == $user->city && $telegram_channel != NULL){
            $tg_channel = DB::table('telegram_channels')->where('city', $user->city)->where('id', '>', $telegram_channel->last_id)->first();

            if ($tg_channel == NULL){
                City::where('city', '=', $user->city)->delete();

                $cities = new City();
                $tg_channel = DB::table('telegram_channels')->where('city', $user->city)->where('status', '=', 1)->first();

                if ($user->status > 0) {
                    $cities->city = $tg_channel->city;
                    $cities->last_id = $tg_channel->id;
                    $cities->save();

                    $this->spam($tg_channel->tg_channel_id, $message);
                }
            }
            else{
                $cities = City::where('city', '=', $user->city)->first();
                $cities->last_id = $tg_channel->id;
                $cities->save();

                $this->spam($tg_channel->tg_channel_id, $message);
            }
        }

        $user->status = "0";
        $user->save();
    }

    public function spam($telegram_id, $message)
    {
        $bot = new BotApi('5537026034:AAFflGtcIrXUbh4Fw-UVMdx_riB7LEMCawY');
        $bot->sendMessage($telegram_id, implode($message));
    }

}
