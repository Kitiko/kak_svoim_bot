<?php

namespace App\Http\Controllers;

use App\Helpers\Telegram;
use App\Helpers\Application;
use App\Models\City;
use App\Repositories\Repository;
use Illuminate\Http\Request;


class ApplicationController
{
    protected Repository $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function callback()
    {

        $utm_medium = $this->repository->getUtm_medium();
        dd($utm_medium);
        $status = $this->repository->getStatus();
//        if () {
//            dd(111);
//        }

//        if (isset($request["message"])) {
//            $textBotCommand = $request["message"]["text"] ?? null;
//            $utm_medium = $request["message"]["text"] ?? null;
//            $chat_id = $request["message"]["chat"]["id"] ?? null;
//            $userName = $request["message"]["chat"]["first_name"] ?? null;
//            if (isset($textBotCommand)) {
//                $message = 'Hi, ' . $userName . '!';
//                $telegram->sendMessage($chat_id, $message);
//            }
//        }
        return response()->json([
            'result' => true
        ], 200);
    }
}
